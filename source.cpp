#include <iostream>
#include <fstream>
#include <string>
#include <Windows.h>
#include <iomanip>

using namespace std;

const string PATH = "Cinemas.txt";

struct Cinema {
	string name;
	string phone;
	string address;
	string workTime;
};

void readData();
void saveData();
void add();
void remove();
void search();
void printAll();
void print(Cinema);

void clearCin() {
	cin.clear();
	cin.ignore(100, '\n');
}

void setColor(int color) {
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consoleHandle, color);
}

Cinema *cinemas;
Cinema cinemaColumnNames;
int size = 0;
int max_size = 100;

int main() {
	setlocale(LC_ALL, "ru");
	setColor(1);

	int action;

	cinemaColumnNames.name = "name";
	cinemaColumnNames.phone = "phone";
	cinemaColumnNames.address = "address";
	cinemaColumnNames.workTime = "workTime";
	
	cinemas = new Cinema[max_size];
	readData();
	
	while(true) {
		system("clear");

		cout << "1. Добавить" << endl;
		cout << "2. Удалить" << endl;
		cout << "3. Поиск" << endl;
		cout << "4. Вывод всех кинотеатров" << endl;
		cout << "5. Сохранить" << endl;
		cout << "6. Выйти" << endl << endl << endl;
		
		cin >> action;

		clearCin();
		system("clear");
		switch(action) {
			case 1:
				add();
				break;
			case 2:
				remove();
				break;
			case 3:
				search();
				break;
			case 4:
				printAll();
				break;
			case 5:
				saveData();
				break;
			case 6:
				return 0;
			default:
				cout << "Выберите от 1 до 5" << endl;
		}
		cin.get();
	}
	
	return 0;
}

void readData() {
	ifstream fin;
	fin.open(PATH);
	
	if(fin.is_open()) {
		int index = 0;
		while(getline(fin, cinemas[index].name)) {
			getline(fin, cinemas[index].phone);
			getline(fin, cinemas[index].address);
			getline(fin, cinemas[index].workTime);
			index++;
		}
		size = index;
		fin.close();
	} else {
		cout << "Не удалось открыть файл" << endl;
	}
}

void saveData() {
	ofstream fout;
	fout.open(PATH);

	if (fout.is_open())
	{
		for(int i = 0; i < size; i++) {
			fout << cinemas[i].name << endl;
			fout << cinemas[i].phone << endl;
			fout << cinemas[i].address << endl;
			fout << cinemas[i].workTime << endl;
		}
		fout.close();
		cout << "Данные сохранены в файл" << endl;
	} else {
		cout << "Не удалось открыть файл" << endl;
	}
}

void add() {
	if (size < max_size - 1) {
		string name, phone, address, workTime;

		cout << "Введите название кинотеатра" << endl;
		getline(cin, name);
		system("clear");
		cout << "Введите номер телефона кинотеатра" << endl;
		getline(cin, phone);
		system("clear");
		cout << "Введите адрес кинотеатра" << endl;
		getline(cin, address);
		system("clear");
		cout << "Введите время работы кинотеатра" << endl;
		getline(cin, workTime);
		system("clear");
		
		size++;
		cinemas[size-1].name = name;
		cinemas[size-1].phone = phone;
		cinemas[size-1].address = address;
		cinemas[size-1].workTime = workTime;
		
		cout << "Кинотеатр добавлен" << endl;
	} else cout << "База заполнена" << endl;
}

void remove() {
	string name;
	cout << "Введите название кинотеатра для удаления" << endl;
	getline(cin, name);
	
	for(int i = 0; i < size; i++) {
		if(cinemas[i].name == name) {
			if(i < size-1) {
				for(int j = i; j < size-1; j++) {
					cinemas[j] = cinemas[j+1];
				}
			}
			size--;
			cout << "Кинотеатр удален" << endl;
			break;
		}
		
		if(i == size-1)
			cout << "Кинотеатр не найден" << endl;
	}
}

void search() {
	string search;
	cout << "Введите название кинотеатра, который хотите найти" << endl;
	getline(cin, search);
	
	print(cinemaColumnNames);
	for(int i = 0; i < size; i++) {
		if(cinemas[i].name.find(search) != string::npos) {
			print(cinemas[i]);
		}
	}
}

void printAll() {
	print(cinemaColumnNames);
	int index = 0;
	setColor(14);
	while(index < size) {
		print(cinemas[index]);
		index++;
	}
	setColor(1);
}

void print(Cinema cinema) {
	cout << setw(25) << left << cinema.name;
	cout << setw(25) << left << cinema.phone;
	cout << setw(25) << left << cinema.address;
	cout << setw(25) << left << cinema.workTime << endl;
}





